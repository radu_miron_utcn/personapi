package edu.utcn.personapi.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author radumiron
 * @since 6/2/21
 */
@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public Person get(String idNumber) {
        return personRepository.findById(idNumber)
                .orElseThrow(() -> new IllegalArgumentException("The person does not exist!"));
    }

    public Person save(Person personEntity) {
        return personRepository.save(personEntity);
    }

    public void delete(String idNumber) {
        personRepository.deleteById(idNumber);
    }

    public Iterable<Person> list() {
        return personRepository.findAll();
    }
}
