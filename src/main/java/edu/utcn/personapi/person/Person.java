package edu.utcn.personapi.person;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

/**
 * @author radumiron
 * @since 6/2/21
 */
@Entity
@Data
public class Person {
    @Id
    @NotEmpty
    private String idNumber;
    private String firstName;
    private String lastName;
}
