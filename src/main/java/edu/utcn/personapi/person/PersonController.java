package edu.utcn.personapi.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author radumiron
 * @since 6/2/21
 */
@RestController
@RequestMapping("/persons")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping
    public Iterable<Person> list() {
        return personService.list();
    }

    @GetMapping("/{idNumber}")
    public Person get(@PathVariable String idNumber) {
        return personService.get(idNumber);
    }

    @PostMapping
    public Person create(@RequestBody Person personEntity) {
        return personService.save(personEntity);
    }

    @PutMapping
    public Person update(@RequestBody Person personEntity) {
        return personService.save(personEntity);
    }

    @DeleteMapping("/{idNumber}")
    public void delete(@PathVariable String idNumber) {
        personService.delete(idNumber);
    }
}
