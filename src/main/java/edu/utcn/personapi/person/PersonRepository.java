package edu.utcn.personapi.person;

import org.springframework.data.repository.CrudRepository;

/**
 * @author radumiron
 * @since 6/2/21
 */
public interface PersonRepository extends CrudRepository<Person, String> {
}
